OCAMLBUILD=ocamlbuild -classic-display \
		-tags annot,debug,thread \
		-libs unix
TARGET=native

all: example server


example:
	$(OCAMLBUILD) example.$(TARGET)

server:
	$(OCAMLBUILD) server.$(TARGET)

clean:
	$(OCAMLBUILD) -clean

realclean: clean
	rm -f *~

cleanall: realclean
