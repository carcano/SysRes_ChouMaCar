type action =
    Atom of ( (unit -> unit) * action)
  | Fork of ( action * action)
  | Stop

type 'a c = ('a -> action) -> action

let action cont = cont (fun _ -> Stop);;

let atom (act,value) cont = Atom (act, cont value);;

let stop cont = Stop;;

let par x1 x2 cont = Fork ((x1 cont), (x2 cont));;

let fork x cont = Fork ((action x), (cont ()));;

let rec round = function
  | [] -> ()
  | x :: xs -> match x with
      | Atom (side_effect, actions) -> side_effect (); round (xs @ [actions])
      | Fork (x1, x2) -> round (xs @ [x1;x2])
      | Stop -> round xs




















