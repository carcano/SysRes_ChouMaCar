module type S = sig
  type 'a process
  type 'a in_port
  type 'a out_port

  val new_channel: unit -> 'a in_port * 'a out_port
  val put: 'a -> 'a out_port -> unit process
  val get: 'a in_port -> 'a process

  val doco: unit process list -> unit process

  val return: 'a -> 'a process
  val bind: 'a process -> ('a -> 'b process) -> 'b process

  val run: 'a process -> 'a
end

module Lib (K : S) = struct

  let ( >>= ) x f = K.bind x f

  let delay f x =
    K.bind (K.return ()) (fun () -> K.return (f x))

  let par_map f l =
    let rec build_workers l (ports, workers) =
      match l with
      | [] -> (ports, workers)
      | x :: l ->
          let qi, qo = K.new_channel () in
          build_workers
            l
            (qi :: ports,
             ((delay f x) >>= (fun v -> K.put v qo)) :: workers)
    in
    let ports, workers = build_workers l ([], []) in
    let rec collect l acc qo =
      match l with
      | [] -> K.put acc qo
      | qi :: l -> (K.get qi) >>= (fun v -> collect l (v :: acc) qo)
    in
    let qi, qo = K.new_channel () in
    K.run
      ((K.doco ((collect ports [] qo) :: workers)) >>= (fun _ -> K.get qi))

end


module Th: S = struct
  type 'a process = (unit -> 'a)

  type 'a channel = { q: 'a Queue.t ; m: Mutex.t; }
  type 'a in_port = 'a channel
  type 'a out_port = 'a channel

  let new_channel () =
    let q = { q = Queue.create (); m = Mutex.create (); } in
    q, q

  let put v c () =
    Mutex.lock c.m;
    Queue.push v c.q;
    Mutex.unlock c.m;
    Thread.yield ()

  let rec get c () =
    try
      Mutex.lock c.m;
      let v = Queue.pop c.q in
      Mutex.unlock c.m;
      v
    with Queue.Empty ->
      Mutex.unlock c.m;
      Thread.yield ();
      get c ()

  let doco l () =
    let ths = List.map (fun f -> Thread.create f ()) l in
    List.iter (fun th -> Thread.join th) ths

  let return v = (fun () -> v)

  let bind e e' () =
    let v = e () in
    Thread.yield ();
    e' v ()

  let run e = e ()
end

module Sim: S = struct
  type 'a result =
    | Continue of (unit -> 'a result)
    | Done of 'a
    | Missing_input of (unit -> 'a result)

  type 'a process = (unit -> 'a result)

  type 'a channel = { q: 'a Queue.t }
  type 'a in_port = 'a channel
  type 'a out_port = 'a channel

  let new_channel () =
    print_string "channel created"; print_newline();
    let q = { q = Queue.create (); } in
    q, q

  let put v c =
    Queue.push v c.q;
    fun () -> (Done ())

  let rec get c =
    try
      let v = Queue.pop c.q in
      fun () -> (Done v)
    with Queue.Empty ->
      fun () -> (Missing_input (get c))

  let getRandEl l =
    let n = List.length l in
    let r = Random.int n in
    print_string "chose "; print_int r; print_newline();
    let rec extract acc i = function
      | [] -> failwith "out of bounds"
      | h::t when i=0 -> (h,acc@t)
      | h::t -> extract (h::acc) (i-1) t
    in extract [] r l

  let doco l =
    let running = ref l in
    while (List.length (!running))>0 do
      (*print_string "running\n";*)
      let p,rest = getRandEl (!running) in
      match (p ()) with
        | Done () -> running := rest
        | Continue f -> running := f::rest
        | Missing_input f -> running := f::rest
    done;
    fun () -> (Done ())

  let return v = (fun () -> (Done v))

  let rec run e =
    (*print_string "run is starting";print_newline();*)
    match e() with
      | Done v -> v
      | Continue c -> print_string "run is continuing...\n"; run c
      | Missing_input i -> failwith "Deadlock"

  let rec bind (e:'a process) (f: 'a->('b process)) : 'b process =
    fun () -> (
      (*print_string "evaluating first arg for bind"; print_newline();*)
      match e () with
        | Done v -> Continue (f v)
        | Continue rest_of_e -> Continue(bind rest_of_e f)
        | Missing_input rest_of_e -> Continue(bind rest_of_e f)
    )

end

module Pipe : S = struct
  type 'a process = (unit -> 'a)

  type 'a in_port = in_channel
  type 'a out_port = out_channel

  let new_channel () =
    let (a,b) = Unix.pipe () in
    (Unix.in_channel_of_descr a, Unix.out_channel_of_descr b)
  ;;

  let get c () =
    Marshal.from_channel c
  ;;

  let put x c () =
    Marshal.to_channel c x []
  ;;

  let doco l () =
    let ths = List.map (fun f -> Thread.create f ()) l in
    List.iter (fun th -> Thread.join th) ths
  ;;

  let return v = (fun () -> v);;

  let bind e e' () =
    let v = e () in
    Thread.yield ();
    e' v ()
  ;;

  let run e = e ()

end

module Net : S = struct
  type 'a process = (unit -> 'a)

  type 'a in_port = in_channel
  type 'a out_port = out_channel

  let new_channel () =
    let (a,b) = Unix.socketpair Unix.PF_UNIX Unix.SOCK_STREAM 0 in
    (Unix.in_channel_of_descr a, Unix.out_channel_of_descr b)
  ;;

  let get c () =
    Marshal.from_channel c
  ;;

  let put x c () =
    Marshal.to_channel c x []
  ;;

  let doco l () =
    let ths = List.map (fun f -> Thread.create f ()) l in
    List.iter (fun th -> Thread.join th) ths
  ;;

  let return v = (fun () -> v);;

  let bind e e' () =
    let v = e () in
    Thread.yield ();
    e' v ()
  ;;

  let run e = e ()

end

module NetII : S = struct
  open Unix

  type 'a process = (unit -> 'a)

  type 'a in_port = int
  type 'a out_port = int

  let chan_num = ref 0 ;;

  let serv_addr = inet_addr_of_string "127.0.0.1";;

  let init_connection () =
    let s = socket PF_INET SOCK_STREAM 0 in
    connect s (ADDR_INET (serv_addr,4242));
    (s,(in_channel_of_descr s,out_channel_of_descr s));;

 let close_connection s =
   shutdown s SHUTDOWN_SEND;
   close s;;

 let new_channel () =
    let res = (!chan_num,!chan_num) in
    incr chan_num;
    res
  ;;

  let get c () =
   let (s,(inC,outC))= init_connection () in
   output_value outC "GET";
   output_value outC c;
   flush outC;
   let res = Marshal.from_channel inC in
   close_connection s;
   res
  ;;

  let put x c () =
    let (s,(_,outC))= init_connection () in
    output_value outC "PUT";
    output_value outC c;
    output_value outC x;
    flush outC
  ;;

  let doco l () =
    let (s,(inC,outC)) = init_connection () in
    let jobsNum = Marshal.from_channel inC in
    let ths =
      List.map
        (fun f -> Thread.create f ())
        (List.map (List.nth l) jobsNum) in
    List.iter (Thread.join) ths
  ;;

  let return v = (fun () -> v);;

  let bind e e' () =
    let v = e () in
    Thread.yield ();
    e' v ()
  ;;

  let run e = e ()

end
