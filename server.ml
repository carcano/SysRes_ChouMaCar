open Unix

let make_addr host port =
  ADDR_INET((gethostbyname host).h_addr_list.(0),port)

(* f est la fonction qui accepte un client *)
(* g est la fonction qui traite la demande d'un client *)
let run_server saddr f g =
  let s = socket PF_INET SOCK_STREAM 0 in
  let _ = bind s saddr in
  listen s 10;
  let nb_clients = 2 in
  let nb_connected = ref 0 in
  while !nb_connected < nb_clients do
    f (accept s) (!nb_connected);
    incr nb_connected
  done;
  print_string "All clients connected"; print_newline();
  while true do
    g (accept s);
  done

let rec getJobList i nClients nJobs =
  if i>=nJobs then []
  else i::(getJobList (i+nClients) nClients nJobs)

(* Donne au client la liste de ses jobs *)
let connect_client (fd,s) id_client =
  let inchannel = in_channel_of_descr fd in
  let outchannel = out_channel_of_descr fd in
  let nb_jobs = input_value inchannel in
  let nb_clients = 2 in
  output_value outchannel (getJobList id_client nb_clients nb_jobs);
  flush outchannel;
  close fd

module IntMap = Map.Make(String)

(* Un map qui associe une queue à chaque int *)
let data = ref IntMap.empty

(* Crée la queue si elle n'existe pas déjà, et push la donnée *)
let put_data j d =
  let i = string_of_int j in  (* J'arrivais pas à faire un map de int alors j'ai fait un map de string *)
  if ((IntMap.mem i (!data))=false) then
    data := IntMap.add i (Queue.create()) (!data);
  Queue.push d (IntMap.find i (!data))

(* Dans ma version de OCaml Bytes n'existe pas encore alors j'ai utilisé String à la place *)
(* Renvoie le statut et la donnée *)
let get_data j =
  let i = string_of_int j in  (* J'arrivais pas à faire un map de int alors j'ai fait un map de string *)
  if ((IntMap.mem i (!data))=false) then
    "EMPTY",""
  else
    let q = IntMap.find i (!data) in
    if (Queue.is_empty q) then
      "EMPTY",""
    else
      "OK",(Queue.pop q)

(* Protocole :
  * Client envoie "PUT" puis un entier puis une donnée
  *
  * -- ou --
  *
  * Client envoie "GET" puis un entier. Puis le serveur répond "OK" puis la donnée, ou "EMPTY"
  *)
let listen_to_client (fd,s) =
  let inchannel = in_channel_of_descr fd in
  let outchannel = out_channel_of_descr fd in
  let request = input_value inchannel in
  if request = "GET" then
    let (status,result) = get_data (input_value inchannel) in
    output_value outchannel status;
    if status="OK" then
      output_value outchannel status;
  else if request = "PUT" then
    output_value outchannel (put_data (input_value inchannel) (input_value inchannel))
  else
    failwith "Erreur de protocole";
  close fd
